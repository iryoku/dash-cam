using Dalamud.Data;
using Dalamud.Game;
using Dalamud.Game.ClientState;
using Dalamud.Game.Command;
using Dalamud.Game.Gui;
using Dalamud.Hooking;
using Dalamud.IoC;
using Dalamud.Logging;
using Dalamud.Plugin;

using FFXIVClientStructs.FFXIV.Client.Game;

using Optional;

using System;
using System.Linq;
using System.Runtime.InteropServices;

namespace DashCam;

internal sealed class Plugin : IDalamudPlugin
{
    private static class SpellAction
    {
        public const uint ElusiveJump = 94;
        public const uint ElusiveJumpPvp = 8803;

        public const uint EnAvant = 16010;
        public const uint EnAvantPvp = 17764;

        public const uint HellsIngress = 24401;
        public const uint HellsEgress = 24402;

        public const uint HellsIngressPvp = 27817;
        public const uint HellsEgressPvp = 27818;
    }

    private static class ActionCategory
    {
        public const uint Spell = 2;
        public const uint Weaponskill = 3;
        public const uint Ability = 4;
    }

    public unsafe Plugin()
    {
        VerifyHooks();

        UseActionHook = Hook<UseActionDelegate>.FromAddress(
            new IntPtr(ActionManager.MemberFunctionPointers.UseAction),
            UseActionCallback);
        UseActionHook.Enable();

        CameraManager = Scanner.GetStaticAddressFromSig(
            "4C 8D 35 ?? ?? ?? ?? 85 D2");
        if (CameraManager == IntPtr.Zero)
        {
            throw new InvalidOperationException(
                "Global CameraManager not found");
        }

        var rotateObjectPtr = Scanner.ScanText(
            "E8 ?? ?? ?? ?? 83 FE 4F");
        if (rotateObjectPtr == IntPtr.Zero)
        {
            throw new InvalidOperationException(
                "RotateObject function not found");
        }
        RotateObject = Marshal
            .GetDelegateForFunctionPointer<RotateObjectDelegate>(
            rotateObjectPtr);

        WindowManager = new();

        WindowManager.AddWindow<ConfigurationWindow>(false);

        Configuration =
            (Configuration?)PluginInterface.GetPluginConfig() ??
            new Configuration();

        PluginInterface.UiBuilder.Draw += Draw;
        PluginInterface.UiBuilder.OpenConfigUi += OpenConfig;

        Command.AddHandler("/dashcam", new(ConfigCommand)
        {
            HelpMessage = "Open the Dash Cam configuration window."
        });
    }

    private void VerifyHooks()
    {
        var parameters = typeof(ActionManager)
            .GetMethod(nameof(ActionManager.UseAction))?.GetParameters()
            .Select(p => p.ParameterType) ?? Enumerable.Empty<Type>();
        if (!parameters.SequenceEqual(new[] {
            typeof(ActionType),
            typeof(uint),
            typeof(long),
            typeof(uint),
            typeof(uint),
            typeof(uint),
            typeof(void*)
        }))
        {
            throw new InvalidOperationException(
                "Hook parameters do not match expected types");
        }
    }

    [PluginService]
    public static DalamudPluginInterface PluginInterface { get; set; } = null!;

    [PluginService]
    private static DataManager Data { get; set; } = null!;

    [PluginService]
    private static ClientState Client { get; set; } = null!;

    [PluginService]
    private static SigScanner Scanner { get; set; } = null!;

    [PluginService]
    private static ChatGui Chat { get; set; } = null!;

    [PluginService]
    private static CommandManager Command { get; set; } = null!;

    public static Configuration Configuration { get; set; }

    private bool Disposed { get; set; }

    private WindowManager WindowManager { get; }

    private Hook<UseActionDelegate> UseActionHook { get; }

    private IntPtr CameraManager { get; }

    private RotateObjectDelegate RotateObject { get; }

    string IDalamudPlugin.Name => "Dash Cam";

    void IDisposable.Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
        if (!Disposed)
        {
            Disposed = true;
            if (disposing)
            {
                Command.RemoveHandler("/dashcam");
                UseActionHook.Disable();
                UseActionHook.Dispose();
            }
        }
    }

    private void Draw()
    {
        WindowManager.Draw();
    }

    private void OpenConfig()
    {
        WindowManager.ShowWindow<ConfigurationWindow>();
    }

    private void ConfigCommand(string command, string arguments)
    {
        if (!string.IsNullOrWhiteSpace(arguments))
        {
            Chat.PrintError("/dashcam");
            Chat.PrintError("    Unknown arguments: " + arguments);
            return;
        }

        OpenConfig();
    }

    private unsafe bool UseActionCallback(
        ref ActionManager manager,
        ActionType type, uint actionId, long targetId,
        uint arg3, uint arg4, uint arg5, IntPtr arg6)
    {
        PluginLog.Debug(
            "UseAction({Type}, {Id}, {Target:X}, {3:X}, {4:X}, {5:X}, {6:X})",
            type, actionId, targetId, arg3, arg4, arg5, arg6.ToInt64());

        var adjustedId = manager.GetAdjustedActionId(actionId);

        var doTurn = false;
        if (type == ActionType.Spell)
        {
            switch (adjustedId)
            {
                case SpellAction.ElusiveJump:
                case SpellAction.ElusiveJumpPvp:
                    doTurn = Configuration.ElusiveJump;
                    break;

                case SpellAction.EnAvant:
                case SpellAction.EnAvantPvp:
                    doTurn = Configuration.EnAvant;
                    break;

                case SpellAction.HellsIngress:
                case SpellAction.HellsIngressPvp:
                    doTurn = Configuration.HellsIngress;
                    break;

                case SpellAction.HellsEgress:
                case SpellAction.HellsEgressPvp:
                    doTurn = Configuration.HellsEgress;
                    break;
            }
        }

        if (doTurn)
        {
            var result = TurnAndUseAction(
                ref manager,
                type, actionId, targetId,
                arg3, arg4, arg5, arg6);
            if (result.HasValue)
            {
                return result.ValueOr(false);
            }
        }

        return UseActionHook.Original(
            ref manager,
            type, actionId, targetId,
            arg3, arg4, arg5, arg6);
    }

    private unsafe Option<bool> TurnAndUseAction(
        ref ActionManager manager,
        ActionType type, uint actionId, long targetId,
        uint arg3, uint arg4, uint arg5, IntPtr arg6)
    {
        if (
            manager.IsRecastTimerActive(type, actionId) ||
            manager.GetActionStatus(type, actionId) != 0)
        {
            return Option.None<bool>();
        }

        var player = Client.LocalPlayer;
        if (player is null)
        {
            return Option.None<bool>();
        }

        ref var cameraManager = ref *(CameraManager*)CameraManager;
        if (cameraManager.WorldCamera is null)
        {
            return Option.None<bool>();
        }

        ref var camera = ref *cameraManager.WorldCamera;
        var rotation = camera.Heading;
        rotation = (rotation + MathF.Tau) % MathF.Tau - MathF.PI;

        RotateObject(player.Address, rotation);

        var result = UseActionHook.Original(
            ref manager,
            type, actionId, targetId,
            arg3, arg4, arg5, arg6);

        return Option.Some(result);
    }

    private delegate bool UseActionDelegate(
        ref ActionManager manager,
        ActionType type, uint actionId, long targetId,
        uint arg4, uint arg5, uint arg6, IntPtr arg7);

    private delegate IntPtr RotateObjectDelegate(IntPtr obj, float rotation);
}

using Dalamud.Interface;

using ImGuiNET;

using System.Numerics;

namespace DashCam;

internal class ConfigurationWindow : Window
{
    protected override string Title => "Dash Cam Configuration";

    protected override Vector2 Position => new(100, 100);

    protected override Vector2 Size => new(300, 300);

    protected override ImGuiWindowFlags Flags =>
        ImGuiWindowFlags.NoResize;

    private Configuration Configuration;

    protected override void Initialize()
    {
        Configuration = Plugin.Configuration;
    }

    protected override void Draw()
    {
        ImGui.Text("Dragoon");
        ImGui.Checkbox("Elusuve Jump", ref Configuration.ElusiveJump);

        ImGui.Separator();

        ImGui.Text("Dancer");
        ImGui.Checkbox("En Avant", ref Configuration.EnAvant);

        ImGui.Separator();

        ImGui.Text("Reaper");
        ImGui.Checkbox("Hell's Ingress", ref Configuration.HellsIngress);
        ImGui.Checkbox("Hell's Egress", ref Configuration.HellsEgress);

        var size = ImGui.GetWindowContentRegionMax();
        var buttonSize = ImGuiHelpers.GetButtonSize("Save and Close");

        ImGui.SetCursorPosY(size.Y - buttonSize.Y);

        if (ImGui.Button("Save"))
        {
            Save();
        }

        ImGui.SameLine();
        if (ImGui.Button("Close"))
        {
            Close();
        }

        ImGui.SameLine();
        if (ImGui.Button("Save and Close"))
        {
            Save();
            Close();
        }
    }

    private void Save()
    {
        Plugin.Configuration = Configuration;
        Plugin.PluginInterface.SavePluginConfig(Configuration);
    }
}

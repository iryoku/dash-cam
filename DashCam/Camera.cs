using System.Runtime.InteropServices;

namespace DashCam;

[StructLayout(LayoutKind.Explicit)]
internal struct Camera
{
    [FieldOffset(0x130)]
    public float Heading;
}

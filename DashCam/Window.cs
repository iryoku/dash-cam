using System.Numerics;

using ImGuiNET;

namespace DashCam;

internal abstract class Window : IWindow
{
    protected abstract string Title { get; }

    protected abstract Vector2 Position { get; }

    protected abstract Vector2 Size { get; }

    protected virtual ImGuiWindowFlags Flags => ImGuiWindowFlags.None;

    private bool Visible { get; set; }

    protected virtual void Initialize() { }

    protected abstract void Draw();

    protected void Close()
    {
        Visible = false;
    }

    bool IWindow.Draw(bool visible)
    {
        if (visible != Visible && visible)
        {
            Initialize();
        }

        if (visible)
        {
            ImGui.SetNextWindowPos(Position, ImGuiCond.FirstUseEver);
            ImGui.SetNextWindowSize(Size, ImGuiCond.FirstUseEver);
            ImGui.PushID(GetType().AssemblyQualifiedName);
            if (ImGui.Begin(Title, ref visible, Flags))
            {
                Visible = visible;
                try
                {
                    Draw();
                }
                finally
                {
                    ImGui.End();
                    ImGui.PopID();
                }
            }
        }
        return Visible;
    }
}


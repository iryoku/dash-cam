using System;
using System.Collections.Generic;
using System.Linq;

namespace DashCam;

internal class WindowManager : IDisposable
{
    private IList<WindowInfo> Windows { get; init; } = new List<WindowInfo>();

    public bool Disposed { get; set; }

    public bool Draw(bool visible = true)
    {
        foreach (var info in Windows)
        {
            info.Visible = info.Instance.Draw(visible && info.Visible);
        }
        return visible;
    }

    public void AddWindow<T>(bool visible = false) where T : IWindow, new() =>
        Windows.Add(new WindowInfo(new T(), visible));

    public void ShowWindow<T>() where T : IWindow =>
        Windows.First(w => w.Instance is T).Visible = true;

    public void HideWindow<T>() where T : IWindow =>
        Windows.First(w => w.Instance is T).Visible = false;

    public void ToggleWindow<T>() where T : IWindow =>
        Windows.First(w => w.Instance is T).Visible ^= true;

    protected virtual void Dispose(bool disposing)
    {
        if (!Disposed)
        {
            Disposed = true;
            if (disposing)
            {
                foreach (var info in Windows)
                {
                    if (info.Instance is IDisposable disposable)
                    {
                        disposable.Dispose();
                    }
                }
            }
        }
    }

    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }

    private class WindowInfo
    {
        public WindowInfo(IWindow instance, bool visible)
        {
            Instance = instance ??
                throw new ArgumentNullException(nameof(instance));
            Visible = visible;
        }

        public IWindow Instance { get; init; }

        public bool Visible { get; set; }
    }
}

using Dalamud.Configuration;

namespace DashCam;

internal struct Configuration : IPluginConfiguration
{
    public int Version = 0;

    public bool ElusiveJump = true;

    public bool EnAvant = true;

    public bool HellsIngress = true;

    public bool HellsEgress = true;

    public Configuration() { }

    int IPluginConfiguration.Version { get => Version; set => Version = value; }
}

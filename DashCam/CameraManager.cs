using System.Runtime.InteropServices;

namespace DashCam;

[StructLayout(LayoutKind.Explicit)]
internal unsafe struct CameraManager
{
    [FieldOffset(0x0)]
    public Camera* WorldCamera;
}

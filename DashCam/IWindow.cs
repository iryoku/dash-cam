namespace DashCam;

internal interface IWindow
{
    bool Draw(bool visible);
}
